#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

const char* ssid = "APLab";
const char* password = "testlabxarxes";
  
ESP8266WebServer server(80);  //Define l'objecte servidor http al port 80

//Handler de les requests del servidor http, cada cop que un client es connecti s'executarà aquesta funció
void handler() {
  String web;

  //Creem un document de json de mida fixa
  StaticJsonDocument<500> json_obj;
  JsonObject root = json_obj.createNestedObject();

  //Omplim el document amb una string i l'adreça mac del servidor
  root["MESSAGE"] = "Test de JSON sobre HTTP";
  root["MAC"] = WiFi.softAPmacAddress();

  serializeJson(root, web);  //Serialitzem l'objecte json a un document html
  server.send(200, "text/html", web); //Diem al servidor http que serveixi el document
}

   
void setup() {
  
  Serial.begin(9600);  //Inicialitzem el serial
  Serial.print("Creating acces point..."); 
  WiFi.mode(WIFI_AP); //Definim el mode de la interfície com a punt d'accés 
  WiFi.softAP(ssid, password); //Configurem l'ssid i password
  Serial.print("Server IP: ");
  Serial.println(WiFi.softAPIP()); //Imprimim per serial la ip del servidor

  server.on("/", handler); //Associem la funció handler a els requests pel directori arrel del servidor
    
  server.begin(); //Inicialitzem el servidor
  Serial.println("HTTP server started");
}

void loop() {
  server.handleClient(); //gestiona les requests que rep el servidor
}

#include <ESP8266WiFi.h>
#include "ThingSpeak.h" //Necessari descarregar la llibreria de thingspeak de 
const char* ssid = "AndroidAP_2477";
const char* pass = "testlabxarxes";

WiFiClient client;

const int channelID = 1595526;
const char* writeAPIKey = "R90ELZK3LQH3POX9";
const char* server = "api.thingspeak.com";
const int postingInterval = 20*1000;

void setup() {
  Serial.begin(115200);  // Initialize serial
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  WiFi.mode(WIFI_STA); 
  ThingSpeak.begin(client);  // Initialize ThingSpeak
}


void loop() {
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }

  
  long rssi = WiFi.RSSI();
  
  int x = ThingSpeak.writeField(channelID, 1, rssi, writeAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }
  
  delay(10000);
}

# -*- coding: utf-8 -*-
"""
@author: Xavi i Aniol
"""

import socket
import threading



HOST = '127.0.0.1'  # Localhost
PORT = 65432        # Port to listen


def recieve(conn):
    while True:
        data = conn.recv(1024).decode()
        print('Client: ', data)
        if not data or data == 'bye':
            return 

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print('Connection with ', addr)
        reciever = threading.Thread(target=recieve, args=((conn,)))
        reciever.start()
        while reciever.is_alive():
            response = input()
            conn.sendall(str.encode(response))
        

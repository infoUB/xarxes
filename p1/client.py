# -*- coding: utf-8 -*-
"""
@author: Xavi
"""

import socket
import threading


HOST = '127.0.0.1'  # Localhost
PORT = 65432        # Port to listen

def recieve(conn):
    while True:
        data = conn.recv(1024).decode()
        print('Server: ', data)
        if not data or data == 'bye':
            return

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    reciever = threading.Thread(target=recieve, args=((s,)))
    reciever.start()
    while reciever.is_alive():
        response = input()
        s.sendall(str.encode(response))
    

            

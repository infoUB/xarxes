 # Exercici 1
 ## Pas 1
 ```
 Packet Tracer PC Command Line 1.0
C:\>ping 127.0.0.1

Pinging 127.0.0.1 with 32 bytes of data:

Reply from 127.0.0.1: bytes=32 time=6ms TTL=128
Reply from 127.0.0.1: bytes=32 time=4ms TTL=128
Reply from 127.0.0.1: bytes=32 time=3ms TTL=128
Reply from 127.0.0.1: bytes=32 time=2ms TTL=128

Ping statistics for 127.0.0.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 2ms, Maximum = 6ms, Average = 3ms

C:\>ping 192.168.1.1

Pinging 192.168.1.1 with 32 bytes of data:

Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255

Ping statistics for 192.168.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>
```

## Pas 2

```
Packet Tracer PC Command Line 1.0
C:\>ping 127.0.0.1

Pinging 127.0.0.1 with 32 bytes of data:

Reply from 127.0.0.1: bytes=32 time<1ms TTL=128
Reply from 127.0.0.1: bytes=32 time=4ms TTL=128
Reply from 127.0.0.1: bytes=32 time=4ms TTL=128
Reply from 127.0.0.1: bytes=32 time=4ms TTL=128

Ping statistics for 127.0.0.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 4ms, Average = 3ms

C:\>ping 161.116.1.1

Pinging 161.116.1.1 with 32 bytes of data:

Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255

Ping statistics for 161.116.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>
```

# Exercici 1
## Des d'un PC de la xarxa 192.168.0.0 a la resta:

Packet Tracer PC Command Line 1.0
C:\>ipconfig /all

FastEthernet0 Connection:(default port)

   Connection-specific DNS Suffix..: 
   Physical Address................: 0030.A398.E604
   Link-local IPv6 Address.........: FE80::230:A3FF:FE98:E604
   IPv6 Address....................: ::
   IPv4 Address....................: 192.168.1.35
   Subnet Mask.....................: 255.255.255.0
   Default Gateway.................: ::
                                     192.168.1.1
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-14-EB-69-08-00-30-A3-98-E6-04
   DNS Servers.....................: ::
                                     192.168.1.1

Bluetooth Connection:

   Connection-specific DNS Suffix..: 
   Physical Address................: 0002.173E.58BE
   Link-local IPv6 Address.........: ::
   IPv6 Address....................: ::
   IPv4 Address....................: 0.0.0.0
   Subnet Mask.....................: 0.0.0.0
   Default Gateway.................: ::
                                     0.0.0.0
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-14-EB-69-08-00-30-A3-98-E6-04
   DNS Servers.....................: ::
                                     192.168.1.1



C:\>ping 192.168.1.1

Pinging 192.168.1.1 with 32 bytes of data:

Reply from 192.168.1.1: bytes=32 time=1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255

Ping statistics for 192.168.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms

C:\>ping 192.168.1.33

Pinging 192.168.1.33 with 32 bytes of data:

Reply from 192.168.1.33: bytes=32 time<1ms TTL=128
Reply from 192.168.1.33: bytes=32 time<1ms TTL=128
Reply from 192.168.1.33: bytes=32 time<1ms TTL=128
Reply from 192.168.1.33: bytes=32 time<1ms TTL=128

Ping statistics for 192.168.1.33:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>ping 161.116.1.1

Pinging 161.116.1.1 with 32 bytes of data:

Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255

Ping statistics for 161.116.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>ping 161.116.1.33

Pinging 161.116.1.33 with 32 bytes of data:

Reply from 161.116.1.33: bytes=32 time<1ms TTL=127
Reply from 161.116.1.33: bytes=32 time<1ms TTL=127
Reply from 161.116.1.33: bytes=32 time<1ms TTL=127
Reply from 161.116.1.33: bytes=32 time<1ms TTL=127

Ping statistics for 161.116.1.33:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>

## Des d'un PC de la xarxa 161.116.0.0 a la resta:
C:\>ipconfig /all

FastEthernet0 Connection:(default port)

   Connection-specific DNS Suffix..: 
   Physical Address................: 000C.855D.293B
   Link-local IPv6 Address.........: FE80::20C:85FF:FE5D:293B
   IPv6 Address....................: ::
   IPv4 Address....................: 161.116.1.33
   Subnet Mask.....................: 255.255.0.0
   Default Gateway.................: ::
                                     161.116.1.1
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-A9-13-4B-E9-00-0C-85-5D-29-3B
   DNS Servers.....................: ::
                                     161.116.1.1

Bluetooth Connection:

   Connection-specific DNS Suffix..: 
   Physical Address................: 0001.9674.ECE3
   Link-local IPv6 Address.........: ::
   IPv6 Address....................: ::
   IPv4 Address....................: 0.0.0.0
   Subnet Mask.....................: 0.0.0.0
   Default Gateway.................: ::
                                     0.0.0.0
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-A9-13-4B-E9-00-0C-85-5D-29-3B
   DNS Servers.....................: ::
                                     161.116.1.1



C:\>ping 161.116.1.1

Pinging 161.116.1.1 with 32 bytes of data:

Reply from 161.116.1.1: bytes=32 time=19ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255
Reply from 161.116.1.1: bytes=32 time<1ms TTL=255

Ping statistics for 161.116.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 19ms, Average = 4ms

C:\>ping 192.168.1.1

Pinging 192.168.1.1 with 32 bytes of data:

Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255
Reply from 192.168.1.1: bytes=32 time<1ms TTL=255

Ping statistics for 192.168.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>ping 192.168.1.33

Pinging 192.168.1.33 with 32 bytes of data:

Reply from 192.168.1.33: bytes=32 time<1ms TTL=127
Reply from 192.168.1.33: bytes=32 time<1ms TTL=127
Reply from 192.168.1.33: bytes=32 time<1ms TTL=127
Reply from 192.168.1.33: bytes=32 time<1ms TTL=127

Ping statistics for 192.168.1.33:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms

C:\>

# Exercici 3

C:\>ipconfig /all

FastEthernet0 Connection:(default port)

   Connection-specific DNS Suffix..: 
   Physical Address................: 000C.855D.293B
   Link-local IPv6 Address.........: FE80::20C:85FF:FE5D:293B
   IPv6 Address....................: ::
   IPv4 Address....................: 161.116.1.33
   Subnet Mask.....................: 255.255.0.0
   Default Gateway.................: ::
                                     161.116.1.1
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-A9-13-4B-E9-00-0C-85-5D-29-3B
   DNS Servers.....................: ::
                                     161.116.1.1

Bluetooth Connection:

   Connection-specific DNS Suffix..: 
   Physical Address................: 0001.9674.ECE3
   Link-local IPv6 Address.........: ::
   IPv6 Address....................: ::
   IPv4 Address....................: 0.0.0.0
   Subnet Mask.....................: 0.0.0.0
   Default Gateway.................: ::
                                     0.0.0.0
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-A9-13-4B-E9-00-0C-85-5D-29-3B
   DNS Servers.....................: ::
                                     161.116.1.1

C:\>ipconfig /release
Port is not using DHCP.
C:\>ipconfig /renew

   IP Address......................: 161.116.0.1
   Subnet Mask.....................: 255.255.0.0
   Default Gateway.................: 161.116.1.1
   DNS Server......................: 161.116.1.1

C:\>ipconfig /all

FastEthernet0 Connection:(default port)

   Connection-specific DNS Suffix..: 
   Physical Address................: 000C.855D.293B
   Link-local IPv6 Address.........: FE80::20C:85FF:FE5D:293B
   IPv6 Address....................: ::
   IPv4 Address....................: 161.116.0.1
   Subnet Mask.....................: 255.255.0.0
   Default Gateway.................: ::
                                     161.116.1.1
   DHCP Servers....................: 161.116.1.1
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-A9-13-4B-E9-00-0C-85-5D-29-3B
   DNS Servers.....................: ::
                                     161.116.1.1

Bluetooth Connection:

   Connection-specific DNS Suffix..: 
   Physical Address................: 0001.9674.ECE3
   Link-local IPv6 Address.........: ::
   IPv6 Address....................: ::
   IPv4 Address....................: 0.0.0.0
   Subnet Mask.....................: 0.0.0.0
   Default Gateway.................: ::
                                     0.0.0.0
   DHCP Servers....................: 0.0.0.0
   DHCPv6 IAID.....................: 
   DHCPv6 Client DUID..............: 00-01-00-01-A9-13-4B-E9-00-0C-85-5D-29-3B
   DNS Servers.....................: ::
                                     161.116.1.1


C:\>


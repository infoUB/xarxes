#include "ESP8266WiFi.h"


//Funció per connectar a una xarxa donat el seu SSID i password
int connect_to_wifi(String ssid, String pwd)
{
  int k=0;

  //En cas de no estar connectats a cap xarxa, intentem der la connexió
  if(WiFi.status() != WL_CONNECTED)
  {
    WiFi.begin(ssid, pwd); //Intentem fer la connexió (entenem que la funció no és bloquejant)
    while(WiFi.status() != WL_CONNECTED) //Si encara no hem aconsequit realitzar la connexió
    {
      delay(1000); //Esperem 1 segon
      Serial.println("Connecting...");
      k++; //Incrementem el contador
      if(k == 10) //En cas d'haver arribat al màxim d'iteracions retornem amb codi d'error -1
      {
        return -1;
      }
    }
    Serial.println(WiFi.localIP()); //Un cop s'hagi realitzat la connexió, imprimim la IP assignada per serial
    return 1;
  }
  else //Si ja estem connectats retornem 0
  {
    return 0;
  }
}

//Mètode per seleccionar una xarxa wifi per serial
void wifi_net_sel(void)
{

  if(WiFi.status() != WL_CONNECTED) //Si no estem connectats a cap xarxa
  {
    scan(); //Busquem les xarxes disponibles i imprimim els seus detalls
    String ssid, pass; //Definim l'ssid i la contrassenya com a String, la funció WiFi.begin els accepta sense problema
    Serial.println("SSID? >");
    while(!Serial.available()); //Bloquejem l'execució fins que el serial estigui disponible
    ssid = Serial.readStringUntil('\n'); //Llegim l'entrada d'usuari. Aquesta funció llegeix fins que fem el retorn de línia i retorna la una String sense null terminator ni \n final. 
    Serial.print("SSID selected > "); 
    Serial.println(ssid);
    delay(1000);
    Serial.print("Password? > "); 
    while(!Serial.available()); //Esperem que el serial estigui disponible i llegim cadena de la contrassenys
    pass = Serial.readStringUntil('\n');
    Serial.print("PWD > ");
    Serial.println(pass);
    delay(1000);
    while(connect_to_wifi(ssid, pass) == -1); //Executem repetidament la funció connect_to_wifi mentre no aconsegueixi connectar-se
  }
  
}

void scan(void)
{
  Serial.println("Scan start");

  int n = WiFi.scanNetworks(); //Escanneja les xarxes disponibles i guarda el nombre de xarxes disponibles
  Serial.println("Scan done");
  if(n == 0) //Si no hi ha cap xarxa, mostra missatge d'error
    Serial.println("no networks found");
  else { //En cas contrari, per a cada xarxa mostra SSID (nom de la xarxa), RSSI (potència rebuda) i indica amb un asterisc si és oberta o no.
    for(int i = 0; i < n; i++){
      Serial.print(i+1);
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(") ");
      Serial.print(WiFi.channel(i));
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ": "*");
      delay(10);
    }
  }
  Serial.println("***********************");
}



void setup() {
  Serial.begin(9600); //Inicialitzem el serial a una veolcitat de 9600 bauds
  WiFi.mode(WIFI_STA); //Inicialitza la interfície de xarxa en mode station (només per connectar-se a xarxes, no fa d'access point)
  WiFi.disconnect(); //Descartem conexions de xarxa que poguéssin estar guardades en memòria
  delay(2000); 
  Serial.println("Setup done");
}

void loop() {
 wifi_net_sel(); //Executem repetidament el mètode per mostrar, seleccionar i connectar-se a xarxes
}
